package m1.miage.istic.star;

import android.app.DownloadManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;


public class MyService extends Service {


    private static final String CHANNEL_ID = "channelID";
    DownloadManager downloadManager;

    String result;
    String dateDebut = "";
    String dateFin = "";
    String url = null;

    @Override
    public void onCreate() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Toast.makeText(getApplicationContext(), "Service started", Toast.LENGTH_LONG).show();
        downloadJSONFile();

        try {
            Thread.sleep(2000);
            readFile();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return START_NOT_STICKY;
    }


    public void readFile() throws IOException {
        int i = 0;
        String url = null;
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/fichier.json");

        FileInputStream stream = new FileInputStream(file);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line = "";
        while (line != null) {
            line = bufferedReader.readLine();
            result = result + line;
        }
        bufferedReader.close();
        String data = result.substring(4);

        try {
            org.json.JSONArray ja = new org.json.JSONArray(data);
            for (i = 0; i < ja.length(); i++) {
                JSONObject jo = (JSONObject) ja.get(i);
                JSONObject joDate = (JSONObject) jo.getJSONObject("fields");
                dateDebut = (String) joDate.getString("debutvalidite");
                dateFin = (String) joDate.getString("finvalidite");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date dateDebutParsed = sdf.parse(dateDebut);
                Date dateFinParsed = sdf.parse(dateFin);
                Calendar calendar1 = Calendar.getInstance();
                Calendar calendar2 = Calendar.getInstance();
                Calendar calendar3 = Calendar.getInstance();
                calendar1.setTime(dateDebutParsed);
                calendar2.setTime(dateFinParsed);
                calendar3.setTime(new Date());

                boolean sameDateDebut = ((calendar3.get(Calendar.YEAR) == calendar1.get(Calendar.YEAR)) &&
                        (calendar3.get(Calendar.MONTH) == calendar1.get(Calendar.MONTH))
                        && (calendar3.get(Calendar.DAY_OF_MONTH) == calendar1.get(Calendar.DAY_OF_MONTH)));

                boolean sameDateFin = ((calendar3.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)) &&
                        (calendar3.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH))
                        && (calendar3.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)));

                if ((new Date().after(dateDebutParsed) || sameDateDebut) && (new Date().before(dateFinParsed) || sameDateFin)) {
                    Toast.makeText(getApplicationContext(), "la date " + (i + 1) + " correspond", Toast.LENGTH_LONG).show();
                    url = joDate.getString("url");
                    break;
                }
            }

        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }
        this.url = url;
        if (url != null) {
            createNotification();
        }


    }

    public void downloadJSONFile() {
        downloadManager = (DownloadManager) getSystemService(getApplicationContext().DOWNLOAD_SERVICE);
        Uri uri = Uri.parse("https://data.explore.star.fr/explore/dataset/tco-busmetro-horaires-gtfs-versions-td/download/?format=json&timezone=Europe/Berlin&lang=fr");
        DownloadManager.Request request = new DownloadManager.Request(uri);
        //request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "fichier.json");
        request.setTitle("Téléchargement du fichier JSON ...");
        Long reference = downloadManager.enqueue(request);

        downloadManager.getUriForDownloadedFile(reference);

        File filepath = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/fichier.json");
        Toast.makeText(getApplicationContext(), "fichier JSON téléchargé avec succès dans:\n" + filepath, Toast.LENGTH_LONG).show();


        //IntentFilter intentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        //registerReceiver(onRead, intentFilter);
    }
/*
    BroadcastReceiver onRead=new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            try {
                readFile();
                //Toast.makeText(getApplicationContext(), "Le fichier JSON téléchargé et lu avec succès", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    };
*/

    public void createNotification() throws IOException {

        final int progressMax = 100;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Intent activityIntent = new Intent(MyService.this, MyActivity.class);
        activityIntent.putExtra("Url file", getUrl());

        PendingIntent pendingIntent = PendingIntent.getActivity(MyService.this, 0, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(MyService.this, CHANNEL_ID)
                .setContentTitle("Horraire Bus STAR")
                .setContentText("Nouveau fichier disponible")
                .setSmallIcon(R.drawable.ic_notification)
                .setColor(Color.BLUE)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                //.setWhen(3000) a voir ce qu'elle fond ces deux fonctions 
                //.setShowWhen(true)
                .addAction(R.mipmap.ic_launcher, "Télécharger", pendingIntent);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            String channelId = CHANNEL_ID;
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "Channel",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
            notification.setChannelId(channelId);
        }

        notificationManager.notify(1, notification.build());
    }

    public String getUrl() {
        return url;
    }

}
