package m1.miage.istic.star.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import m1.miage.istic.star.models.Trip;

@Dao
public interface Trip_Dao {

    @Query("SELECT * FROM Trip WHERE trip_id = :trip_id")
    LiveData<List<Trip>> getTrip(long trip_id);

    @Insert
    long insertTrip(Trip trip);

    @Update
    int updateTrip(Trip trip);

    @Query("DELETE FROM Trip WHERE trip_id = :trip_id")
    int deleteTrip(String trip_id);
}
