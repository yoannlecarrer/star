package m1.miage.istic.star;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.room.Room;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import m1.miage.istic.star.R;
import m1.miage.istic.star.database.SaveMyDatabase;

import m1.miage.istic.star.models.Calendar;

public class MainActivity extends AppCompatActivity {
    TextView editTextInput;
    DownloadManager downloadManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //DatabaseInitializer.populateAsync(SaveMyDatabase.getInstance(this));

        //ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.FOREGROUND_SERVICE}, PackageManager.PERMISSION_GRANTED);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                //permission denied, request it
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);

            } else {
                //permission already granted, perform download
                Intent intent = new Intent(this, MyService.class);
                startService(intent);
                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                }*/
            }
        } else {
            Intent intent = new Intent(this, MyService.class);
            startService(intent);
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(intent);
            }*/
        }
/*
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Calendar calendar = new Calendar("203110000","1","1","1","1","1","1","1","20201102","20201218");

                long calendar1 = SaveMyDatabase.getInstance(getApplicationContext()).calendar_dao().insertCalendar(calendar);
                Toast.makeText(getApplicationContext(),"calendar1",Toast.LENGTH_LONG).show();
            }
        });
*/

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case 100:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(this, MyService.class);
                    startService(intent);
                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startForegroundService(intent);
                   }*/

                } else {
                    //permission denied from pop up
                    Toast.makeText(getApplicationContext(), "Permission denied from pop up", Toast.LENGTH_LONG).show();
                }

        }
    }


}