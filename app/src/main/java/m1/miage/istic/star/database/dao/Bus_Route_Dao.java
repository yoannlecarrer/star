package m1.miage.istic.star.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import m1.miage.istic.star.models.Bus_Route;

@Dao
public interface Bus_Route_Dao {

    @Query("SELECT * FROM Bus_route WHERE route_id = :route_id")
    LiveData<List<Bus_Route>> getBusRoute(int route_id);

    @Insert
    long insertBusRoute(Bus_Route route);

    @Update
    int updateBusRoute(Bus_Route route);

    @Query("DELETE FROM Bus_route WHERE route_id = :route_id")
    int deleteBusRoute(String route_id);

}
