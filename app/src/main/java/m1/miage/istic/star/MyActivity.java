package m1.miage.istic.star;


import android.Manifest;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.webkit.DownloadListener;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.room.Room;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import m1.miage.istic.star.database.SaveMyDatabase;
import m1.miage.istic.star.models.Calendar;

public class MyActivity extends AppCompatActivity {
    DownloadManager downloadManagerBD;
    private static  String[][] tabCalendar = new String[50][10];
    private static  String[][] tabBus = new String[50][7];
    private static  String[][] tabStop = new String[50][7];
    private static  String[][] tabStop_times = new String[50][6];
    private static  String[][] tabTrip = new String[50][7];
    static final String SEPARATEUR = ",";

    // FOR DATA
    private SaveMyDatabase database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final String url = getIntent().getStringExtra("Url file");

        downloadBD(url);


    }

    public void initDb() throws Exception {
        this.database = Room.databaseBuilder(this, SaveMyDatabase.class, "db").build();
    }

    public void closeDb() throws Exception {
        database.close();
    }

    public void insertCalendarTab() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i < tabCalendar.length; i++) {
                    for (int j = 0; j < tabCalendar[i].length; j += 9) {
                        Calendar calendar = new Calendar(tabCalendar[i][j], tabCalendar[i][j + 1], tabCalendar[i][j + 2], tabCalendar[i][j + 3]
                                , tabCalendar[i][j + 4], tabCalendar[i][j + 5], tabCalendar[i][j + 6], tabCalendar[i][j + 7], tabCalendar[i][j + 8], tabCalendar[i][j + 9]);
                        SaveMyDatabase.getInstance(MyActivity.this).calendar_dao().insertCalendar(calendar);
                    }

                }
            }
        }).start();
    }


    public void downloadBD(String url) {

            downloadManagerBD = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            Uri uri = Uri.parse(url);
            DownloadManager.Request request = new DownloadManager.Request(uri);
            //Toast.makeText(getApplicationContext(),  uri.toString(), Toast.LENGTH_LONG).show();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "myDB.zip");
            request.setTitle("Téléchargement du fichier zip ...");
            Long reference = downloadManagerBD.enqueue(request);

            downloadManagerBD.getUriForDownloadedFile(reference);
            File filepath = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/myDb.zip");
            //Toast.makeText(getApplicationContext(), "Fichier de la base de données est téléchargé avec succès dans:\n" + filepath, Toast.LENGTH_LONG).show();
            registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            extractFilesFromZip();
            Toast.makeText(getApplicationContext(), "La base de données est bien insérée", Toast.LENGTH_LONG).show();
        }
    };


    public void extractFilesFromZip() {
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/myDb.zip");
        try {
            FileInputStream fin = new FileInputStream(file);
            ZipInputStream zin = new ZipInputStream(new BufferedInputStream(fin));
            ZipEntry ze = null;
            while ((ze = zin.getNextEntry()) != null) {
                String fileName = ze.getName();

                if (ze.isDirectory()) {
                    dirChecker(ze.getName());
                } else {
                    FileOutputStream fout = new FileOutputStream(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/" + fileName);

                    byte[] buffer = new byte[8192];
                    int len;
                    while ((len = zin.read(buffer)) != -1) {
                        fout.write(buffer, 0, len);
                    }
                    fout.close();

                    zin.closeEntry();

                }

            }
            zin.close();
        } catch (Exception e) {
            Log.e("Decompress", "unzip", e);
        }
        Toast.makeText(getApplicationContext(), "Le fichier de la base de données téléchargé et dézipé avec succès", Toast.LENGTH_LONG).show();


        // Après avoir unzip les fichiers, on insère dans la base de données
        DatabaseInitializer.populateAsync(SaveMyDatabase.getInstance(this));


    }

    private void dirChecker(String dir) {
        File f = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS).getAbsolutePath());
        if (!f.isDirectory()) {
            f.mkdirs();
        }
    }


    public static String[][] readCalendar() throws IOException {
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/calendar.txt");
            FileInputStream stream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            int j = 0;
            String ligne;
            while (bufferedReader.ready()) {
                ligne = bufferedReader.readLine();
                String mots[] = ligne.split(SEPARATEUR);
                for (int i = 0; i < mots.length; i++) {
                    if (i == 0) {
                        Log.v("readCalendar", "mots : " + mots[i]);
                        tabCalendar[j][i] = mots[i];
                    }
                    if (i == 1) {
                        tabCalendar[j][i] = mots[i];
                    }
                    if (i == 2) {
                        tabCalendar[j][i] = mots[i];
                    }
                    if (i == 3) {
                        tabCalendar[j][i] = mots[i];
                    }
                    if (i == 4) {
                        tabCalendar[j][i] = mots[i];
                    }
                    if (i == 5) {
                        tabCalendar[j][i] = mots[i];
                    }
                    if (i == 6) {
                        tabCalendar[j][i] = mots[i];
                    }
                    if (i == 7) {
                        tabCalendar[j][i] = mots[i];
                    }
                    if (i == 8) {
                        tabCalendar[j][i] = mots[i];
                    }
                    if (i == 9) {
                        tabCalendar[j][i] = mots[i];
                    }
                }
                j++;
                Log.v("readCalendar", "ligne : " + ligne);
                Log.v("readCalendar", "" + Arrays.deepToString(tabCalendar));
            }
            bufferedReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.v("tab", " " + tabCalendar);
        return tabCalendar;
    }


    static String[][] readBus() throws IOException {
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/routes.txt");
            FileInputStream stream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            String ligne;
            int j = 0;
            while (bufferedReader.ready()) {
                ligne = bufferedReader.readLine();
                String mots[] = ligne.split(SEPARATEUR);
                for (int i = 0; i < mots.length; i++) {
                    if (i == 0) {
                        tabBus[j][i] = mots[i];
                    }
                    if (i == 2) {
                        tabBus[j][1] = mots[i];
                    }
                    if (i == 3) {
                        tabBus[j][2] = mots[i];
                    }
                    if (i == 4) {
                        tabBus[j][3] = mots[i];
                    }
                    if (i == 5) {
                        tabBus[j][4] = mots[i];
                    }
                    if (i == 7) {
                        tabBus[j][5] = mots[i];
                    }
                    if (i == 8) {
                        tabBus[j][6] = mots[i];
                    }
                }
                j++;
                Log.v("readBus", "ligne : " + ligne);
                Log.v("readBus", "" + Arrays.deepToString(tabBus));

            }
            bufferedReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tabBus;
    }

    static String[][] readStop() throws IOException {
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/stops.txt");
            FileInputStream stream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            String ligne;
            int j = 0;
            while (bufferedReader.ready()) {
                ligne = bufferedReader.readLine();
                String mots[] = ligne.split(SEPARATEUR);
                for (int i = 0; i < mots.length; i++) {
                    if (i == 1) {
                        tabStop[j][0] = mots[i];
                    }
                    if (i == 2) {
                        tabStop[j][1] = mots[i];
                    }
                    if (i == 3) {
                        tabStop[j][2] = mots[i];
                    }
                    if (i == 4) {
                        tabStop[j][3] = mots[i];
                    }
                    if (i == 5) {
                        tabStop[j][4] = mots[i];
                    }
                    if (i == 11) {
                        tabStop[j][5] = mots[i];
                    }
                }
                j++;
                Log.v("readStop", "ligne : " + ligne);
                Log.v("readStop", "" + Arrays.deepToString(tabStop));
            }
            bufferedReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tabStop;
    }

    public static String[][] readStopTime() throws IOException {
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/stop_times.txt");
            FileInputStream stream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            String ligne;
            int j = 0;
            while (bufferedReader.ready()) {
                ligne = bufferedReader.readLine();
                String mots[] = ligne.split(SEPARATEUR);
                for (int i = 0; i < mots.length; i++) {
                    if (i == 0) {
                        tabStop_times[j][i] = mots[i];
                    }
                    if (i == 1) {
                        tabStop_times[j][i] = mots[i];
                    }
                    if (i == 2) {
                        tabStop_times[j][i] = mots[i];
                    }
                    if (i == 3) {
                        tabStop_times[j][i] = mots[i];
                    }
                    if (i == 4) {
                        tabStop_times[j][i] = mots[i];
                    }
                }
                j++;
                Log.v("readStopTime", "ligne : " + ligne);
                Log.v("readStopTime", "" + Arrays.deepToString(tabStop_times));
            }
            bufferedReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tabStop_times;
    }

    static String[][] readTrip() throws IOException {
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/trips.txt");
            FileInputStream stream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            String ligne;
            int j = 0;
            while (bufferedReader.ready()) {
                ligne = bufferedReader.readLine();
                String mots[] = ligne.split(SEPARATEUR);
                for (int i = 0; i < mots.length; i++) {
                    if (i == 0) {
                        tabTrip[j][i] = mots[i];
                    }
                    if (i == 1) {
                        tabTrip[j][i] = mots[i];
                    }
                    if (i == 2) {
                        tabTrip[j][i] = mots[i];
                    }
                    if (i == 3) {
                        tabTrip[j][i] = mots[i];
                    }
                    if (i == 5) {
                        tabTrip[j][4] = mots[i];
                    }
                    if (i == 6) {
                        tabTrip[j][5] = mots[i];
                    }
                    if (i == 8) {
                        tabTrip[j][6] = mots[i];
                    }
                }
                j++;
                Log.v("readTrip", "ligne : " + ligne);
                Log.v("readTrip", "" + Arrays.deepToString(tabTrip));
            }
            Log.v("jyjyjyjyjyjyj", "" + Arrays.deepToString(tabTrip));
            bufferedReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tabTrip;
    }

}
