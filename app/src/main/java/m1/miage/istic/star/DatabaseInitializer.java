package m1.miage.istic.star;


import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import m1.miage.istic.star.database.SaveMyDatabase;
import m1.miage.istic.star.models.Bus_Route;
import m1.miage.istic.star.models.Calendar;
import m1.miage.istic.star.models.Stop;
import m1.miage.istic.star.models.Stop_Time;
import m1.miage.istic.star.models.Trip;

public class DatabaseInitializer {

    private static final String TAG = DatabaseInitializer.class.getName();
    private static  String[][] tabCalendar = new String[50][10];
    static final String SEPARATEUR = ",";

    public static void populateAsync(final SaveMyDatabase db) {
        PopulateDbAsync task = new PopulateDbAsync(db);
        task.execute();
    }

    public static void populateSync(final SaveMyDatabase db) { populateWithTestData(db); }

    private static void populateWithTestData(SaveMyDatabase db) {
        Log.v("TEST","TEST");
        //Calendar calendar = new Calendar("20311000019","0","0","0","1","0","0","0","20201105","20201217");
        //addCalendar(db,calendar);
        /*for (int i = 1; i < tabBus.length; i++) {
            Bus_Route bus_route = new Bus_Route(tabBus[i][0], tabBus[i][1], tabBus[i][2], tabBus[i][3], tabBus[i][4], tabBus[i][5], tabBus[i][6]);
            addBus_Route(db, bus_route);
        }*/
        try {
            insertBusRouteTab (db ,MyActivity.readBus());
            insertStopTab (db ,MyActivity.readStop());
            insertStopTimeTab (db ,MyActivity.readStopTime());
            insertTripTab (db ,MyActivity.readTrip());
            insertCalendarTab (db ,MyActivity.readCalendar());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Calendar addCalendar(final SaveMyDatabase db, Calendar calendar) {
        db.calendar_dao().insertCalendar(calendar);
        return calendar;
    }

    private static Bus_Route addBus_Route(final SaveMyDatabase db, Bus_Route bus_Route) {
        db.bus_route_dao().insertBusRoute(bus_Route);
        return bus_Route;
    }

    private static Stop addStop(final SaveMyDatabase db, Stop stop) {
        db.stop_dao().insertStop(stop);
        return stop;
    }

    private static Stop_Time addStop_Time(final SaveMyDatabase db, Stop_Time stop_time) {
        db.stop_time_dao().insertStop_Time(stop_time);
        return stop_time;
    }

    private static Trip addTrip(final SaveMyDatabase db, Trip trip) {
        db.trip_dao().insertTrip(trip);
        return trip;
    }

    public static void insertCalendarTab(SaveMyDatabase db, final String[][] tabCalendar) {
        for (int i = 1; i < tabCalendar.length; i++) {
            db.calendar_dao().deleteCalendar(tabCalendar[i][0]);
            Calendar calendar = new Calendar(tabCalendar[i][0], tabCalendar[i][1], tabCalendar[i][2], tabCalendar[i][3]
                    , tabCalendar[i][4], tabCalendar[i][5], tabCalendar[i][6], tabCalendar[i][7], tabCalendar[i][8], tabCalendar[i][9]);
            Log.v("",""+calendar);
            addCalendar(db, calendar);
        }
    }

    public static void insertBusRouteTab(SaveMyDatabase db, final String[][] tabBus) {
        for (int i = 1; i < tabBus.length; i++) {
            db.bus_route_dao().deleteBusRoute(tabBus[i][0]);
            Bus_Route bus_route = new Bus_Route(tabBus[i][0], tabBus[i][1], tabBus[i][2], tabBus[i][3], tabBus[i][4], tabBus[i][5], tabBus[i][6]);
            addBus_Route(db, bus_route);
        }
    }

    public static void insertStopTab(SaveMyDatabase db, final String[][] tabStop) {
        for (int i = 1; i < tabStop.length; i++) {
            db.stop_dao().deleteStop(tabStop[i][0]);
            Stop stop = new Stop(tabStop[i][0], tabStop[i][1], tabStop[i][2], tabStop[i][3], tabStop[i][4], tabStop[i][5]);
            addStop(db, stop);
        }
    }

    public static void insertStopTimeTab(SaveMyDatabase db, final String[][] tabStop_times) {
        for (int i = 1; i < tabStop_times.length; i++) {
            db.stop_time_dao().deleteStop_Time(tabStop_times[i][0]);
            Stop_Time stop_time = new Stop_Time(tabStop_times[i][0], tabStop_times[i][1], tabStop_times[i][2], tabStop_times[i][3], tabStop_times[i][4]);
            addStop_Time(db, stop_time);
        }
    }

    public static void insertTripTab(SaveMyDatabase db, final String[][] tabTrip) {
        for (int i = 1; i < tabTrip.length; i++) {
            db.trip_dao().deleteTrip(tabTrip[i][0]);
            Trip trip = new Trip(tabTrip[i][0], tabTrip[i][1], tabTrip[i][2], tabTrip[i][3], tabTrip[i][4], tabTrip[i][5], tabTrip[i][6]);
            addTrip(db, trip);
        }
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final SaveMyDatabase mDb;

        PopulateDbAsync(SaveMyDatabase db) {
            mDb = db;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            populateWithTestData(mDb);
            return null;
        }

        /*@Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }*/
    }



}
