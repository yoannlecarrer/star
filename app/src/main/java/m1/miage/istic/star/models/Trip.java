package m1.miage.istic.star.models;

import android.content.ContentValues;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity
public class Trip {

    @PrimaryKey@NonNull
    private String trip_id;

    private String route_id;
    private String service_id;
    private String trip_headsign;
    private String direction_id;
    private String block_id;
    private String wheelchair_accessible;

    // --- GETTER ---

    public String getTrip_id() { return trip_id; }
    public String getRoute_id() { return route_id; }
    public String getService_id() { return service_id; }
    public String getTrip_headsign() { return trip_headsign; }
    public String getDirection_id() { return direction_id; }
    public String getBlock_id() { return block_id; }
    public String getWheelchair_accessible() { return wheelchair_accessible; }

    // --- SETTER ---

    public void setTrip_id(String trip_id) { this.trip_id = trip_id; }
    public void setRoute_id(String route_id) { this.route_id = route_id; }
    public void setService_id(String service_id) { this.service_id = service_id; }
    public void setTrip_headsign(String trip_headsign) { this.trip_headsign = trip_headsign; }
    public void setDirection_id(String direction_id) { this.direction_id = direction_id; }
    public void setBlock_id(String block_id) { this.block_id = block_id; }
    public void setWheelchair_accessible(String wheelchair_accessible) { this.wheelchair_accessible = wheelchair_accessible; }

    // --- UTILS ---
    public static Trip fromContentValues(ContentValues values) {
        final Trip item = new Trip();
        if (values.containsKey("trip_id")) item.setTrip_id(values.getAsString("trip_id"));
        if (values.containsKey("route_id")) item.setRoute_id(values.getAsString("route_id"));
        if (values.containsKey("service_id")) item.setService_id(values.getAsString("service_id"));
        if (values.containsKey("trip_headsign")) item.setTrip_headsign(values.getAsString("trip_headsign"));
        if (values.containsKey("direction_id")) item.setDirection_id(values.getAsString("direction_id"));
        if (values.containsKey("block_id")) item.setBlock_id(values.getAsString("block_id"));
        if (values.containsKey("wheelchair_accessible")) item.setWheelchair_accessible(values.getAsString("wheelchair_accessible"));
        return item;
    }
}