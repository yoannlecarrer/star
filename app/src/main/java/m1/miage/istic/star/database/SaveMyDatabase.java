package m1.miage.istic.star.database;

import android.content.ContentValues;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.OnConflictStrategy;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
//import fr.istic.mob.tp3.ui.main.MainViewModel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import m1.miage.istic.star.database.dao.*;
import m1.miage.istic.star.models.Bus_Route;
import m1.miage.istic.star.models.Calendar;
import m1.miage.istic.star.models.Stop;
import m1.miage.istic.star.models.Stop_Time;
import m1.miage.istic.star.models.Trip;

@Database(entities = {Bus_Route.class, Calendar.class, Stop.class, Stop_Time.class, Trip.class}, version = 1, exportSchema = false)
public abstract class SaveMyDatabase extends RoomDatabase {

    // --- SINGLETON ---
    private static volatile SaveMyDatabase INSTANCE;

    private static final int NUMBER_OF_THREADS = 1;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    // --- DAO ---
    public abstract Bus_Route_Dao bus_route_dao();
    public abstract Calendar_Dao calendar_dao();
    public abstract Stop_Dao stop_dao();
    public abstract Stop_Time_Dao stop_time_dao();
    public abstract Trip_Dao trip_dao();

    // --- INSTANCE ---
    public static SaveMyDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (SaveMyDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            SaveMyDatabase.class, "MyDatabase")
                            .addCallback(populateDatabase())
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    // -----
    private static Callback populateDatabase() {
            return new Callback() {

                @Override
                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                    super.onCreate(db);

                    /*ContentValues contentValues = new ContentValues();
                    contentValues.put("service_id", "   20311000");
                    contentValues.put("monday", "1");
                    contentValues.put("tuesday", "1");
                    contentValues.put("wednesday", "1");
                    contentValues.put("thursday", "1");
                    contentValues.put("friday", "1");
                    contentValues.put("saturday", "1");
                    contentValues.put("sunday", "1");
                    contentValues.put("start_date", "201218");
                    contentValues.put("end_date", "201102");
                    System.out.println("test");
                    db.calendar_dao().deleteCalendar("2031100000");
                    db.insert("Calendar", OnConflictStrategy.IGNORE, contentValues);*/
                }
            };
        }
    }

