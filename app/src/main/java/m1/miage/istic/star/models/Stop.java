package m1.miage.istic.star.models;

import android.content.ContentValues;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity
public class Stop {

    @PrimaryKey@NonNull
    private String stop_code ;

    private String stop_name;
    private String stop_desc;
    private String stop_lat;
    private String stop_lon;
    private String wheelchair_boarding;

    // --- GETTER ---

    public String getStop_code() { return stop_code; }
    public String getStop_name() { return stop_name; }
    public String getStop_desc() { return stop_desc; }
    public String getStop_lat() { return stop_lat; }
    public String getStop_lon() { return stop_lon; }
    public String getWheelchair_boarding() { return wheelchair_boarding; }

    // --- SETTER ---

    public void setStop_code(String stop_code) { this.stop_code = stop_code; }
    public void setStop_name(String stop_name) { this.stop_name = stop_name; }
    public void setStop_desc(String stop_desc) { this.stop_desc = stop_desc; }
    public void setStop_lat(String stop_lat) { this.stop_lat = stop_lat; }
    public void setStop_lon(String stop_lon) { this.stop_lon = stop_lon; }
    public void setWheelchair_boarding(String wheelchair_boarding) { this.wheelchair_boarding = wheelchair_boarding; }

    // --- UTILS ---
    public static Stop fromContentValues(ContentValues values) {
        final Stop item = new Stop();
        if (values.containsKey("stop_code")) item.setStop_code(values.getAsString("stop_code"));
        if (values.containsKey("stop_name")) item.setStop_name(values.getAsString("stop_name"));
        if (values.containsKey("stop_desc")) item.setStop_desc(values.getAsString("stop_desc"));
        if (values.containsKey("stop_lat")) item.setStop_lat(values.getAsString("stop_lat"));
        if (values.containsKey("stop_lon")) item.setStop_lon(values.getAsString("stop_lon"));
        if (values.containsKey("wheelchair_boarding")) item.setWheelchair_boarding(values.getAsString("wheelchair_boarding"));
        return item;
    }

}