package m1.miage.istic.star.contenu;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public interface StarContract {

    String CONTENT_AUTHORITY  = "m1.miage.istic.star.contenu.StarProvider";

    Uri BASE_CONTENT_URI  = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_BUSROUTE = "busroutes";
    public static final String PATH_CALENDAR = "calendar";
    public static final String PATH_STOPS = "stops";
    public static final String PATH_STOPSTIME = "stopstime";
    public static final String PATH_TRIP = "trip";

    String CONTENT_BUSROUTE = "vnd.android.cursor.dir/vnd.m1.miage.istic.star.contenu.busroutes";
    String CONTENT_ITEM_BUSROUTE = "vnd.android.cursor.item/vnd.m1.miage.istic.star.contenu.busroutes";

    String CONTENT_CALENDAR = "vnd.android.cursor.dir/vnd.m1.miage.istic.star.contenu.calendar";
    String CONTENT_ITEM_CALENDAR = "vnd.android.cursor.item/vnd.m1.miage.istic.star.contenu.calendar";

    String CONTENT_STOPS = "vnd.android.cursor.dir/vnd.m1.miage.istic.star.contenu.stops";
    String CONTENT_ITEM_STOPS = "vnd.android.cursor.item/vnd.m1.miage.istic.star.contenu.stops";

    String CONTENT_STOPSTIME = "vnd.android.cursor.dir/vnd.m1.miage.istic.star.contenu.stopstime";
    String CONTENT_ITEM_STOPSTIME = "vnd.android.cursor.item/vnd.m1.miage.istic.star.contenu.stopstime";

    String CONTENT_TRIP = "vnd.android.cursor.dir/vnd.m1.miage.istic.star.contenu.trip";
    String CONTENT_ITEM_TRIP = "vnd.android.cursor.item/vnd.m1.miage.istic.star.contenu.trip";

    public static final class BusRoutes implements BaseColumns {
        public static final String TABLE_NAME = "Bus_Route";

        public static final String COLUMN_ROUTE_ID = "route_id";
        public static final String COLUMN_ROUTE_SHORT_NAME = "route_short_name";
        public static final String COLUMN_ROUTE_LONG_NAME = "route_long_name";
        public static final String COLUMN_ROUTE_DESC = "route_desc";
        public static final String COLUMN_ROUTE_TYPE = "route_type";
        public static final String COLUMN_ROUTE_COLOR = "route_color";
        public static final String COLUMN_ROUTE_TEXT_COLOR = "route_text_color";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_BUSROUTE).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY +
                        "/" + PATH_BUSROUTE;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY +
                        "/" + PATH_BUSROUTE;

        // Helper method.
        public static Uri buildBusRoutes(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }

    public static final class Calendar implements BaseColumns {

        public static final String TABLE_NAME = "Calendar";

        public static final String COLUMN_SERVICE_ID = "service_id";
        public static final String COLUMN_MONDAY = "monday";
        public static final String COLUMN_TUESDAY = "tuesday";
        public static final String COLUMN_WEDNESDAY = "wednesday";
        public static final String COLUMN_THURSDAY = "thursday";
        public static final String COLUMN_FRIDAY = "friday";
        public static final String COLUMN_SATURDAY = "saturday";
        public static final String COLUMN_STAR_DATE = "start_date";
        public static final String COLUMN_END_DATE = "end_date";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CALENDAR).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY +
                        "/" + PATH_CALENDAR;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY +
                        "/" + PATH_CALENDAR;

        // Helper method.
        public static Uri buildCalendar(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class Stops implements BaseColumns {

        public static final String TABLE_NAME = "Stop";

        public static final String COLUMN_STOP_CODE = "stop_code";
        public static final String COLUMN_STOP_NAME = "stop_name";
        public static final String COLUMN_STOP_DESC = "stop_desc";
        public static final String COLUMN_STOP_LAT = "stop_lat";
        public static final String COLUMN_STOP_LON = "stop_lon";
        public static final String COLUMN_WHEELCHAIR_BOARDING = "wheelchair_boarding";


        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_STOPS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY +
                        "/" + PATH_STOPS;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY +
                        "/" + PATH_STOPS;

        // Helper method.
        public static Uri buildStops(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class StopsTimes implements BaseColumns {
        public static final String TABLE_NAME = "Stop_Time";

        public static final String COLUMN_STOP_ID = "stop_id";
        public static final String COLUMN_TRIP_ID = "trip_id";
        public static final String COLUMN_ARRIVAL_TIME = "arrival_time";
        public static final String COLUMN_DEPARTURE_TIME = "departure_time";
        public static final String COLUMN_STOP_SEQUENCE = "stop_sequence";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_STOPSTIME).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY +
                        "/" + PATH_STOPSTIME;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY +
                        "/" + PATH_STOPSTIME;

        // Helper method.
        public static Uri buildStopsTimes(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class Trip implements BaseColumns {
        public static final String TABLE_NAME = "Trip";

        public static final String COLUMN_ROUTE_ID = "route_id";
        public static final String COLUMN_SERVICE_ID = "service_id";
        public static final String COLUMN_TRIP_HEADSIGN = "trip_headsign";
        public static final String COLUMN_DIRECTION_ID = "direction_id";
        public static final String COLUMN_BLOCK_ID = "block_id";
        public static final String COLUMN_WHEELCHAIR_ACCESSIBLE = "wheelchair_accessible";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_TRIP).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY +
                        "/" + PATH_TRIP;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY +
                        "/" + PATH_TRIP;

        // Helper method.
        public static Uri buildTrip(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
